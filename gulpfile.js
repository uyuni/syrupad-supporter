var gulp = require('gulp'),
  clean = require('gulp-clean'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  minifyhtml = require('gulp-minify-html');

  gulp.task('clean:dist', function() {
    return gulp.src('dist/**/*.*')
      .pipe(clean());
  });

gulp.task('jshint', function() {
  return gulp.src('app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('uglify', function() {
  return gulp.src('app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});

gulp.task('minifyhtml', function() {
  return gulp.src('app/**/*.html')
    .pipe(minifyhtml())
    .pipe(gulp.dest('dist'));
});

gulp.task('copyimage', function() {
  return gulp.src('app/**/*.png')
    .pipe(gulp.dest('dist'));
});

gulp.task('copymanifest', function() {
  return gulp.src('app/manifest.json')
    .pipe(gulp.dest('dist'));
});

gulp.task('copycss', function() {
  return gulp.src('app/**/*.css')
    .pipe(gulp.dest('dist'));
});


gulp.task('default', ['clean:dist', 'uglify', 'minifyhtml', 'copyimage', 'copymanifest', 'copycss']);
