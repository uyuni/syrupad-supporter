'use strict';

var requestUrl, queryStr;
var exceptionList = ['adNo', 'm_iframe', 'callback', 'dummy'];

function getUrlQueryString(url) {
  var obj = {}, tmp = '';
  if(url.indexOf('?') > -1) {
    var query = url.split('?')[1].split('&');
    for(var i=0,il=query.length;i<il;i++) {
      tmp = query[i].split('=');
      obj[tmp[0]] = tmp[1];
    }
  }
  return obj;
}

chrome.runtime.sendMessage({
  'status': 'init'
}, function(response) {
  for(var i=0,il=response.indicator.length;i<il;i++) {
    var inEl = document.getElementById(response.indicator[i]);
    inEl.style.display = 'inline';
    if(response.url[i].indexOf('ad_request') > -1) {
      requestUrl = response.url[i];
    }
  }
  queryStr = getUrlQueryString(requestUrl);
  var tableEl = document.getElementById('publishSetValue');
  var tbodyEl = tableEl.getElementsByTagName('TBODY')[0];
  var p, htmlStr = '';
  for(p in queryStr) {
    if(exceptionList.indexOf(p) < 0) {
      htmlStr += '<tr><th scope="row">' + p +'</th><td>' + queryStr[p] +'</td></tr>';
    }
  }
  tbodyEl.innerHTML = htmlStr;
  tableEl.style.display = 'block';
});
