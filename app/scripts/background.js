'use strict';

var requests = {
  indicator : [],
  url : []
};

chrome.webRequest.onCompleted.addListener(
  function(details) {
    var indicator = '';
    if(details.url.indexOf('tad.min.js') > -1) {
      chrome.pageAction.show(details.tabId);
      indicator = 'sdk';
    } else if (details.url.indexOf('getpolicy') > -1) {
      indicator = 'policy';
    } else if (details.url.indexOf('ad_request') > -1){
      indicator = 'req';
    } else if (details.url.indexOf('html5') > -1){
      indicator = 'adc';
    } else if (details.url.indexOf('mweblog') > -1){
      indicator = 'vlog';
    }
    if(indicator !== '') {
      requests.indicator.push(indicator);
      requests.url.push(details.url);
    }
  },
  {
    urls: [
      'http://*.adotsolution.com/*',
      'https://*.adotsolution.com/*'
    ]
  },
  ['responseHeaders']
);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if(request.status === 'init') {
      sendResponse(requests);
    }
  }
);
